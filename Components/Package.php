<?php

namespace AllSrvs\Components;

class Package {
    const DISTRIBUTION_PATH = "dist";

    /**
     * @return string
     */
    public static function getBaseDir(): string {
       return getcwd();
    }

    /**
     * @return string
     */
    public static function getDistributionDir(): string {
        return self::getBaseDir() . "\\" . self::DISTRIBUTION_PATH . "\\" . self::getVersion();
    }

    /**
     * @return object
     */
    public static function getConfiguration(): object {
        $content = file_get_contents(self::getBaseDir() .'/composer.json');
        return (object) json_decode($content,true);
    }

    public static function getVersion(): string {
        $config = self::getConfiguration();
        return isset($config->version) ? $config->version : 'v1';
    }

    public static function cleanCache(): void {
        $cachePath = realpath(self::getDistributionDir());
        if($cachePath !== false) {
            if(is_dir($cachePath)) {
                self::removeDir($cachePath);
            } else {
                echo "Removing file '$cachePath'";
                unlink($cachePath);
            }
        }
    }

    private static function removeDir($path) {
        if(empty($path)) {
            return false;
        }
        echo "Removing dir '$path'\n";
        exec(sprintf("rd /s /q %s", escapeshellarg($path)));
    }

    public static function loadRepositoryInCache(): void {
        self::cleanCache();
        $repo = str_replace(array("\n", "\t", "\r"), '', `git config --get remote.origin.url`);
        $target = self::getDistributionDir();
        `git clone $repo $target`;
    }

    public static function buildDistribution(): void {
        self::loadRepositoryInCache();
        self::removeDir(self::getDistributionDir() . "\\.git");
        unlink(self::getDistributionDir() . "\\.gitignore");
        $target = self::getDistributionDir();
        `composer install --no-ansi --no-dev --no-progress --no-scripts --optimize-autoloader -d $target`;
    }
}
