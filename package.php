<?php
declare(strict_types=1);
namespace AllSrvs;
require_once 'Components/Package.php';

use AllSrvs\Components\Package;

Package::buildDistribution();

